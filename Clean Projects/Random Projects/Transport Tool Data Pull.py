import selenium
import pandas as pd
import pyodbc
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
import pickle
import os
from glob import glob
import shutil
def TT_datapull():
    extracted_files = os.listdir(r'C:\Users\Riley.Powell\Desktop\TTData')
    driver_name = "chromedriver_win32"
    driver_path = r"C:/Users/riley.powell/Documents/%s/chromedriver.exe" % driver_name
    chrome_options = webdriver.ChromeOptions()
    # driver = webdriver.Chrome(executable_path=driver_path, options=chrome_options)
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get("https://user.wl-netcrackertt-p.aws.dishcloud.io/ncobject.jsp?id=9164167554813252701&tab=_My+Tasks")
    time.sleep(15)
    #navigate to partner management
    driver.find_element(By.XPATH,'/html/body/div[1]/div[3]/ul/li[1]/a').click()
    time.sleep(5)
    driver.find_element(By.ID,'9159653810613294400').click()
    time.sleep(10)
    # This all works beautifully
    contracts_list = []
    num_rows = len(driver.find_elements(By.XPATH,'/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/div[4]/div[2]/table/tbody/tr'))
    # Iterate over each row in the partner management table.
    for vendor in range(37,num_rows):
        path = '/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/div[4]/div[2]/table/tbody/tr[{}]/td[2]/div/div/a'.format(vendor+1)
        time.sleep(5)
        driver.get('https://user.wl-netcrackertt-p.aws.dishcloud.io/ncobject.jsp?id=9159524727113250600')
        # Try clicking each partner, sometimes this doensn't work for some reason. Possibly due to slow connection?
        time.sleep(10)
        vendor_name = driver.find_element(By.XPATH,path).text
        print('Opening vendor '+driver.find_element(By.XPATH,path).text)
        time.sleep(10)
        driver.find_element(By.XPATH, path).click()
        time.sleep(10)
        partner_accounts = len(
            driver.find_elements(By.XPATH, '/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/table/tbody/tr'))
        for account in range(partner_accounts):
            partner_path = '/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/table/tbody/tr[{}]/td[2]/div/div/a'.format(
                account + 1)
            #save the partner accounts_url
            partner_accounts_url = driver.current_url
            # Open the Partner account that is up next
            driver.find_element(By.XPATH, partner_path).click()
            time.sleep(10)
            # Open the contracts tab of the partner account
            driver.find_element(By.XPATH, '/html/body/div[6]/div[2]/ul[1]/li[3]/a').click()
            time.sleep(10)
            # get the contracts in an iterable list
            contracts = len(driver.find_elements(By.XPATH,
                                                 '/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/table/tbody/tr'))
            # Click the Contracts tab and then iterate over every contract in the contracts tab.
            for contract in range(contracts):
                contract_path = '/html/body/div[6]/div[3]/div[1]/div[1]/div[1]/div[2]/div/table/tbody/tr[{}]/td[2]/div/div/a'.format(
                    contract + 1)
                contracts_page_url = driver.current_url

                contracts_list.append(driver.find_element(By.XPATH, contract_path).text)
                contract_name = driver.find_element(By.XPATH, contract_path).text
                PBE_filename = 'PBE_' + vendor_name + '_' + contract_name + '.xlsx'
                # If the contract is an MSA we ignore it, otherwise we will try to pull all of the data out of the table.
                if 'MSA' in driver.find_element(By.XPATH, contract_path).text:
                    continue
                elif PBE_filename in extracted_files:
                    continue
                else:
                    print('Looking at contract: ' + driver.find_element(By.XPATH, contract_path).text)
                    driver.find_element(By.XPATH,contract_path).click()
                    time.sleep(10)
                    # Open the "Others" drop down menu
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[2]/ul[2]/li/span/span[1]').click()
                    time.sleep(5)


                    # Exporting the summary page. This is a more complicated bit as it needs to hover over the export
                    # all option and then click in the dropdown of the submenu.
                    menu_item = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"/html/body/div[6]/div[2]/ul[2]/li/ul/li[6]")))
                    ActionChains(driver).move_to_element(menu_item).perform()

                    WebDriverWait(driver, 20).until(EC.element_to_be_clickable(
                        (By.XPATH, "/html/body/div[6]/div[2]/ul[2]/li/ul/li[6]/ul/li[3]/a"))).click()
                    time.sleep(10)
                    # Reload the page to allow the download to pop up
                    driver.refresh()
                    time.sleep(10)
                    # Click the download button
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div[1]/div/a[2]').click()
                    time.sleep(5)
                    # Close the download notification
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div[1]/div/a[1]').click()
                    time.sleep(5)
                    # Move the most recent download to a new file
                    list_of_files = glob.glob(r'C:\Users\Riley.Powell\Downloads\*.xlsx')  # * means all if need specific format then *.csv
                    latest_file = max(list_of_files, key=os.path.getctime)
                    PBE_filename = 'PBE_'+vendor_name+'_'+contract_name+'.xlsx'
                    os.rename(latest_file,os.path.join(r'C:\Users\Riley.Powell\Downloads',PBE_filename))
                    shutil.move(r'C:\Users\Riley.Powell\Downloads\{}'.format(PBE_filename),r'C:\Users\Riley.Powell\Desktop\TTData\{}'.format(PBE_filename))


                    # Navigate to purchased products
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[2]/ul[1]/li[6]/a').click()
                    time.sleep(5)
                    # open the others dropdown menu
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[2]/ul[2]/li/span/span[1]').click()
                    time.sleep(5)

                    # Hover over the export all link in the dropdown
                    menu_item = WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.XPATH,"/html/body/div[6]/div[2]/ul[2]/li/ul/li[6]")))
                    ActionChains(driver).move_to_element(menu_item).perform()
                    # Click the XLSX option in the exposed submenu
                    WebDriverWait(driver, 20).until(EC.element_to_be_clickable(
                        (By.XPATH, "/html/body/div[6]/div[2]/ul[2]/li/ul/li[6]/ul/li[3]/a"))).click()
                    time.sleep(10)
                    driver.refresh()
                    time.sleep(5)
                    # Click the download button
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div[1]/div/a[2]').click()
                    time.sleep(5)
                    # Close the download notification
                    driver.find_element(By.XPATH,'/html/body/div[6]/div[3]/div[1]/div/a[1]').click()
                    time.sleep(5)

                    list_of_files = glob.glob(
                        r'C:\Users\Riley.Powell\Downloads\*.xlsx')  # * means all if need specific format then *.csv
                    latest_file = max(list_of_files, key=os.path.getctime)
                    PP_filename = 'PP_' + vendor_name + '_' + contract_name + '.xlsx'
                    os.rename(latest_file, os.path.join(r'C:\Users\Riley.Powell\Downloads', PP_filename))
                    shutil.move(r'C:\Users\Riley.Powell\Downloads\{}'.format(PP_filename),
                                r'C:\Users\Riley.Powell\Desktop\TTData\{}'.format(PP_filename))
                    driver.get(contracts_page_url)
                    time.sleep(10)
            driver.get(partner_accounts_url)
            time.sleep(10)


        time.sleep(10)

    # with open('Contracts','wb') as fp:
    #     pickle.dump(contracts_list,fp)

# test = pd.read_excel(r'C:\Users\Riley.Powell\Desktop\TTData\PBE_123.net, Inc._NET_MSO_001.xlsx',index_col=0)
# data = test.loc['Price Book Entry':'Notes']
# data = data.drop([data.index[0],data.index[-1]])
# data.columns = data.iloc[0]
# data = data.drop([data.index[0]])

# data.to_excel(r'C:\Users\Riley.Powell\Desktop\TTtest.xlsx')
def PBE_extraction(directory):
    files = glob(directory)
    for file in files:
        full_file = pd.read_excel(file,index_col=0)
        file_name = file.split('\\')[-1]
        # get just the table we're interested in
        table = full_file.loc['Price Book Entry':'Notes']
        table = table.drop([table.index[0],table.index[-1]])
        table.columns = table.iloc[0]
        table = table.drop([table.index[0]])
        new_path = os.path.join(r'C:\Users\Riley.Powell\Desktop\TTCleanedData\PBE',file_name)
        table.to_excel(new_path)
    return print('All files have been cleaned succesfully')

# PBE_extraction(r'C:\Users\Riley.Powell\Desktop\TTData\PBE*.xlsx')


def PP_extraction(directory):
    files = glob(directory)
    for file in files:
        full_file = pd.read_excel(file, index_col=0)
        file_name = file.split('\\')[-1]
        # get just the table we're interested in
        table = full_file.loc['Purchased Products':]
        table = table.drop([table.index[0]])
        table.columns = table.iloc[0]
        table = table.drop([table.index[0]])
        new_path = os.path.join(r'C:\Users\Riley.Powell\Desktop\TTCleanedData\PP', file_name)
        table.to_excel(new_path)
    return print('All files have been cleaned succesfully')
# PP_extraction(r'C:\Users\Riley.Powell\Desktop\TTData\PP*.xlsx')


# The data preparation steps have been completed. (Hopefully)

pp_files = os.listdir(r'C:\Users\Riley.Powell\Desktop\TTCleanedData\PP')
for file in pp_files:
    vendor = file.split('_')[1]
    print(file)
    file_path = os.path.join(r'C:\Users\Riley.Powell\Desktop\TTCleanedData\PP',file)
    temp = pd.read_excel(file_path)
    temp['Vendor'] = vendor
    if file == 'PP_123.net, Inc._NET_MSO_001.xlsx':
        data = pd.read_excel(file_path)
    else:
        data = pd.concat([data,temp],ignore_index=True)

print(data.head())
print(data.info())
data.to_csv(r'C:\Users\Riley.Powell\Desktop\DataDump.csv')


