import os
import sys

import numpy as np
import pandas as pd

filepath = r'C:\Users\Riley.Powell\Desktop\Example_CF.csv'

df = pd.read_csv(filepath)

iterable = df.to_numpy()
previous_siteID = iterable[0,0]
previous_data = iterable[0,1]
for i in range(len(iterable)):
    if isinstance(iterable[i][0],float):
        iterable[i][0] = previous_siteID
        iterable[i][1] = previous_data
    else:
        previous_siteID = iterable[i][0]
        previous_data = iterable[i][1]
df = pd.DataFrame(iterable,columns = ['Site Name','NTP','Date Submitted'])
df.to_csv(r'C:\Users\Riley.Powell\Desktop\organized.csv')

