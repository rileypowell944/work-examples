from atlassian import Jira
from jira import JIRA
import requests
from requests.auth import HTTPBasicAuth
import json
import pandas as pd
from pprint import pprint
import pickle

def string_unpacking(ls):
    str = ""
    for i in ls:
        str += "'"+i+"',"
    str = str[:-1]
    return str

def Jira_Candidate_change_siteID_pull():
    url = ''
    jira = Jira(url = '',username='',password='',cloud=True,verify_ssl=False)

    try:
        with open('siteIDs.pkl','rb') as f:
            siteIDs = pickle.load(f)
        with open('candidate_change_keys.pkl', 'rb') as f:
            candidate_change_keys = pickle.load(f)
        with open('searched_issues.pkl', 'rb') as f:
            searched_issues = pickle.load(f)
    except:
        siteIDs = []
        candidate_change_keys = []
        searched_issues = []


    if siteIDs:
        query = "project = 'Requests' and type = 'task' and key not in ({})".format(string_unpacking(searched_issues))
    else:
        query = "project = 'Requests' and type = 'task' and 'Request type' = 'Candidate Change'"


    all_issues = jira.jql(query)
    for issue in all_issues['issues']:
        searched_issues.append(issue['key'])
        if issue['fields']['customfield_10236'] != None:
            candidate_change_keys.append(issue['key'])
            siteIDs.append(issue['fields']['customfield_10236'])
    df_searched_issues = pd.DataFrame({'Searched tickets':searched_issues})
    df_searched_issues.to_csv(r'A:\My Drive\My Projects\JiraData\searched_issues_for_candidate_changes.csv')
    df_candidate_change_issues = pd.DataFrame({'Searched tickets':candidate_change_keys})
    df_candidate_change_issues.to_csv(r'A:\My Drive\My Projects\JiraData\candidate_change_keys.csv')
    df_siteIDs = pd.DataFrame({'Searched tickets':siteIDs})
    df_siteIDs.to_csv(r'A:\My Drive\My Projects\JiraData\candidate_change_siteIDs.csv')
    with open('siteIDs.pkl','wb') as f:
        pickle.dump(siteIDs,f)
    with open('candidate_change_keys.pkl','wb') as f:
        pickle.dump(candidate_change_keys,f)
    with open('searched_issues.pkl','wb') as f:
        pickle.dump(searched_issues,f)
    print('All tickets have been pulled.')


