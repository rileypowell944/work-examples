from flask import Flask, request,jsonify, render_template, request, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from markupsafe import escape
import os
from flask_table import Table,Col
from sqlalchemy import MetaData
import urllib
import pandas as pd
import numpy as np
import docx
import dataframe_image as dfi
from werkzeug.utils import secure_filename
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import cast
import sqlalchemy



connection_string = urllib.parse.quote_plus("DRIVER={ODBC Driver 17 for SQL Server};"
                                                    "SERVER="
                                                    "DATABASE="
                                                    "Trusted_Connection=yes;"
                                                    "AUTOCOMMIT=True")
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "mssql+pyodbc:///?odbc_connect={}".format(connection_string)
db = SQLAlchemy(app)

def naming_convention(vendor,contract_type,amd_name=None,mpa_number=None):
    conn = create_engine("mssql+pyodbc:///?odbc_connect={}".format(connection_string), pool_pre_ping=True)
    if contract_type != 'AMD':
        if contract_type == 'MPA':
            mpa_number = mpa_number.zfill(3)
            last_contract = pd.read_sql_query(
                "select top 1 MSO_ID from mso where vendor = '{}' and mso_id not like '%[_]AMD%' and mso_id like '%[_]{}[_]{}%' order by [MSO Effective date] desc".format(
                    vendor, contract_type,mpa_number), con=conn)
        else:
            last_contract = pd.read_sql_query("select top 1 MSO_ID from mso where vendor = '{}' and mso_id not like '%[_]AMD%' and mso_id not like '%[_]MPA%'  and mso_id like '%[_]{}%' order by [MSO Effective date] desc".format(vendor,contract_type),con=conn)
        if last_contract.empty:
            contract_end = '001'
        else:
            contract_end = str(int(last_contract.iloc[0,0][-3:])+1).zfill(3)
        vendor_code = pd.read_sql_query("select top 1 vendorID from vendors where vendorNXO = '{}'".format(vendor),con=conn)
        vendor_code = vendor_code.iloc[0,0]
        if contract_type == 'MPA':
            new_contract = vendor_code + '_' + contract_type + "_" + mpa_number + "_MSO_"+contract_end
        else:
            new_contract = vendor_code + "_" + contract_type + "_" + contract_end
        return new_contract
    else:
        amd_number = amd_name[-3:]
        amd_type = amd_name.split('_')[1]
        mpa_number = amd_name.split("_")[2]
        amd_number = amd_number.zfill(3)
        if amd_type != "MPA":
            last_contract = pd.read_sql_query(
                "select top 1 MSO_ID from mso where vendor = '{}' and mso_id like '%[_]AMD%' and mso_id like '%[_]{}[_]{}%' order by [MSO Effective date] desc".format(
                    vendor, amd_type,amd_number), con=conn)
        else:
            mpa_contract_type = amd_name.split("_")[3]
            mpa_contract_number = amd_name.split("_")[4]
            last_contract = pd.read_sql_query(
                "select top 1 MSO_ID from mso where vendor = '{}' and mso_id like '%[_]AMD%' and mso_id like '%[_]{}[_]{}[_]{}[_]{}%' order by [MSO Effective date] desc".format(
                    vendor, amd_type, mpa_number,mpa_contract_type,mpa_contract_number), con=conn)

        if last_contract.empty:
            contract_end = '001'
        else:
            contract_end = str(int(last_contract.iloc[0, 0][-3:]) + 1).zfill(3)
        vendor_code = pd.read_sql_query("select top 1 vendorID from vendors where vendorNXO = '{}'".format(vendor),
                                        con=conn)
        vendor_code = vendor_code.iloc[0, 0]
        if amd_type != 'MPA':
            new_contract = vendor_code + "_" + amd_type+ "_" + amd_number + "_" + contract_type + "_" + contract_end
        else:
            new_contract = vendor_code + "_" + amd_type + "_" + mpa_number.zfill(3) + "_" + mpa_contract_type + "_" + mpa_contract_number + "_" + contract_type + "_" + contract_end
        return new_contract



class vendors(db.Model):
    Vendor_names = db.Column("VendorNXO",db.String,primary_key=True)
    vendor_type = db.Column("Vendor Type",db.String)
    vendorID = db.Column(db.String)

class Source(db.Model):
    # __table__ = 'Source'
    SiteID = db.Column(db.String,primary_key=True)
    Status = db.Column(db.String)
    LaunchCategory = db.Column(db.String)
    FiberProvider = db.Column(db.String)
    ContractName = db.Column(db.String)
    DCAddress = db.Column(db.String)
    FiberType = db.Column(db.String)
    RingID = db.Column(db.String)

class contract_tool_v2(db.Model):
    SiteID = db.Column("Site ID",db.String,primary_key=True)
    Contract_Name = db.Column("TT Contract Name",db.String)
    DeliveryInterval = db.Column("Delivery Interval",db.String)
    MRC = db.Column('MRC ($)',db.String)
    NRC = db.Column('NRC ($)',db.String)
    Lease_term = db.Column('Lease Term (months)',db.String)
    NXOFiberProvider = db.Column(db.String)
    ContractedFiberProvider = db.Column(db.String)
    Status = db.Column(db.String)
    Region = db.Column(db.String)
    Market = db.Column(db.String)
    AOI = db.Column(db.String)
    FiberType = db.Column('Fiber Type',db.String)
    NXOLaunchCategory = db.Column(db.String)
    TR020AC = db.Column(db.DATE)
    TR050AC = db.Column(db.DATE)
    TR080AC = db.Column(db.DATE)
    ProjectedInstallDate = db.Column("Contracted Install Date",db.DATE)
    ServiceDelay = db.Column("Service Delay",db.String)
    ServiceCredits = db.Column("Service Credits ($)",db.String)
    ETL_payable = db.Column("ETL Payable ($)")
    Terminable = db.Column(db.String)
    Effective_Date = db.Column("MSO Effective date",db.DATETIME)
    MPA_effective_date = db.Column('MPA Effective date',db.DATETIME)



class MSO(db.Model):
    ID = db.Column("MSO_ID",db.String(11),primary_key=True)
    Vendor = db.Column(db.String)
    Effective_Date = db.Column("MSO Effective date",db.DATETIME)
    Term = db.Column("Term (months)",db.Integer)
    Sites_Added = db.Column("Sites Added",db.String)
    MSA_ID = db.Column(db.String)
    hyperlink = db.Column(db.String)
    Design_phase_length_days = db.Column(db.String)
    deselect_pct_dish = db.Column(db.String)
    deselect_vendor_fee = db.Column(db.String)


class MSA(db.Model):
    Contract_Name = db.Column("MSA_ID",db.String,primary_key=True)
    Vendor = db.Column(db.String)
    Effective_date = db.Column(db.DATETIME)
    hyperlink = db.Column(db.String)
    Autoacceptance_period = db.Column(db.String)
    Design_phase_length = db.Column(db.String)
    Resolution_of_Design_Rejections = db.Column(db.String)
    Payment_terms_days = db.Column(db.String)
    Dispute_period = db.Column(db.String)
    Minimum_commitments = db.Column(db.String)
    deselect_pct_vendor = db.Column(db.String)
    Transition_period = db.Column(db.String)
    upgrade_pricing = db.Column('10 gig price upgrade',db.String)


class MPA(db.Model):
    MPA_ID = db.Column(db.String,primary_key=True)
    Vendor = db.Column(db.String)
    Effective_Date = db.Column("MPA_effective_date",db.DATETIME)
    Market = db.Column(db.String)
    hyperlink = db.Column(db.String)
    MSA_ID = db.Column(db.String)
    AMD_MSA_ID = db.Column(db.String)
    Service_description = db.Column(db.String)
    Fees = db.Column(db.String)
    Minimum_service_commitment = db.Column(db.String)
    Cum_NRC_subsidy = db.Column("Cum_NRC_subsidy$",db.String)

class Sitehistory_contract_tool(db.Model):
    SiteID = db.Column("SiteID",db.String,primary_key=True)
    Vendor = db.Column(db.String)
    Contract_Name = db.Column("TT Contract Name",db.String)
    EffectiveDate = db.Column("Contract Effective Date",db.DATETIME,primary_key=True)
    DeliveryInterval = db.Column("Delivery Interval",db.String)
    Term = db.Column('Lease Term (months)',db.Integer)
    MRC = db.Column('MRC ($)',db.String)
    NRC = db.Column('NRC ($)',db.String)
    FiberType = db.Column("Fiber Type",db.String)
    PortSpeed = db.Column("Port Speed",db.String)
    CIR = db.Column(db.String)
    Type = db.Column(db.String)

class vwdeselectpenalty(db.Model):
    Vendor = db.Column('VendorNM',db.String)
    Contract_name = db.Column('ContractNM',db.String,primary_key=True)
    TotalDeselectVendorCNT = db.Column(db.String)
    TotalDeselectDishCNT = db.Column(db.String)
    DeselectVendorPCT =  db.Column(db.String)
    DeselectDishPCT = db.Column(db.String)
    VendorDeselectPenaltyPCT = db.Column(db.String)
    VendorDeselectPenaltyAMT = db.Column(db.String)
    VendorPenaltyAMT =  db.Column(db.String)
    DishDeselectPenaltyPCT = db.Column(db.String)
    DishPenaltyAMT = db.Column('Dish Deselect Penalty',db.String)


@app.route('/')
def home():
    return render_template('home_page.html')



@app.route('/sites',methods=["GET","POST"])
def sites():
    if request.method == "GET":
        sites = contract_tool_v2.query.limit(1).all()
        Site_history = Sitehistory_contract_tool.query.filter(Sitehistory_contract_tool.SiteID.in_(list(sites[0].SiteID.split()))).order_by(Sitehistory_contract_tool.SiteID)
        return render_template('All_Sites.html')
    if request.method == "POST":
        sites = contract_tool_v2.query.filter(contract_tool_v2.SiteID.in_(request.form["fname"].split())).order_by(contract_tool_v2.SiteID)
        Site_history = Sitehistory_contract_tool.query.filter(Sitehistory_contract_tool.SiteID.in_(request.form['fname'].split())).order_by(Sitehistory_contract_tool.SiteID)
        price_upgrade = MSA.query.filter(MSA.Vendor.like(Source.query.filter(Source.SiteID.like(request.form['fname'])).with_entities(Source.FiberProvider))).with_entities(MSA.upgrade_pricing)
        return render_template('individual_Sites.html',current_sites=sites, current_sites_columns = list(contract_tool_v2.__table__.columns),site_history = Site_history,site_history_columns = list(Sitehistory_contract_tool.__table__.columns),price_upgrade=price_upgrade)





@app.route('/MSO',methods=["GET","POST"])
def MSOs():
    if request.method == "GET":
        contracts = MSO.query.with_entities(MSO.ID,MSO.hyperlink).order_by(MSO.ID).all()
        return render_template("all_MSO.html",contracts=contracts,columns=list(MSO.__table__.columns))
    if request.method == "POST":
        conn = create_engine("mssql+pyodbc:///?odbc_connect={}".format(connection_string), pool_pre_ping=True)
        originally_awarded_sites = pd.read_sql_query("select count(siteid) as [sites1] from sitehistory where [Contract Name] = '{}' and type in ('addition','modification')".format(request.form['fname']),con=conn)
        current_sites_in_contract = pd.read_sql_query("select  count(  [Site ID]) as [sites2] from [New Vendor Sites] where [TT Contract Name] = '{}' ".format(request.form['fname']),con=conn)
        contract_value = pd.read_sql_query("select format(sum(cast([monthly_mrc] as float))+sum(cast([monthly_NRC] as float)),'c') as TCV from Sourcing_Financial_Analysis where [TT_Contract_Name] = '{}'".format(request.form['fname']),con=conn)
        ETL_credits = pd.read_sql_query("select format(sum(cast([ETL_Payable] AS INT)),'C') AS etl_payable from ETL_credits where [TT Contract Name] = '{}'".format(request.form['fname']),con=conn)
        vendor_code = request.form['fname'].split('_')[0]
        deselect_info = vwdeselectpenalty.query.filter(vwdeselectpenalty.Contract_name.in_(request.form['fname'].split()))
        total_service_credits = pd.read_sql_query("Select format(sum([Service Credits ($)]),'C') as credits from service_credits where [tt contract name] = '{}'".format(request.form['fname']),con=conn)
        contracts = MSO.query.filter(MSO.ID.in_(request.form["fname"].split()))
        site_info = contract_tool_v2.query.filter(contract_tool_v2.Contract_Name.in_(request.form["fname"].split())).order_by(contract_tool_v2.SiteID)
        price_upgrade = MSA.query.filter(MSA.Contract_Name.like('%{}%'.format(vendor_code))).with_entities(MSA.upgrade_pricing)
        return render_template("individual_MSOs.html",contract_info=contracts,site_info=site_info,columns=list(contract_tool_v2.__table__.columns),originally_awarded=originally_awarded_sites,current_sites = current_sites_in_contract,TCV = contract_value,etl=ETL_credits,price_upgrade=price_upgrade,deselect_info = deselect_info,service_credits=total_service_credits)#,number_deselected_vendor=number_deselected_vendor,percent_deselected_vendor = percent_deselected_vendor,number_deselected_dish=number_deselected_dish,percent_deselect_dish=percent_deselect_dish,vendor_deselect_penalty=vendor_penalty,dish_deselect_penalty=dish_deselect_penalty)



@app.route('/MSA',methods=['GET','POST'])
def MSAs():
    if request.method == "GET":
        links = MSA.query.with_entities(MSA.hyperlink,MSA.Contract_Name).order_by(MSA.Contract_Name).all()
        return render_template("all_MSAs.html",links=links)
    if request.method == "POST":
        conn = create_engine("mssql+pyodbc:///?odbc_connect={}".format(connection_string), pool_pre_ping=True)
        originally_awarded_sites = pd.read_sql_query("select count(  s.[SiteID]) as [sites1] from sitehistory s  join MSA m on s.vendor=m.vendor where type = 'Addition' and m.msa_id = '{}'  group by s.[vendor],m.msa_id order by s.[vendor]".format(request.form['fname']),con=conn)
        current_sites_in_contract = pd.read_sql_query("select  count(  s.[Site ID]) as [sites2] from [New Vendor Sites] s join MSA m on s.[FiberProvider]=m.vendor  where m.msa_id = '{}' group by [FiberProvider] order by [FiberProvider]".format(request.form['fname']),con=conn)
        # vendor_deselected_sites = pd.read_sql_query("select count(  s.[SiteID]) as [sites1] from sitehistory s  join MSA m on s.vendor=m.vendor where s.type like '%deselect vendor%' and m.msa_id = '{}'  group by s.[vendor],m.msa_id order by s.[vendor]".format(request.form['fname']),con=conn)
        # dish_deselected_sites = pd.read_sql_query("select count(  s.[SiteID]) as [sites1] from sitehistory s  join MSA m on s.vendor=m.vendor where s.type like '%deselect dish%' and m.msa_id = '{}'  group by s.[vendor],m.msa_id order by s.[vendor]".format(request.form['fname']),con=conn)
        vendor_code = request.form['fname'].split('_')[0]
        vendor_code = '%'+vendor_code+'%'
        deselect_info = vwdeselectpenalty.query.filter(
            vwdeselectpenalty.Contract_name.like(vendor_code)).with_entities(func.sum(cast(func.coalesce(vwdeselectpenalty.TotalDeselectVendorCNT,0),sqlalchemy.Integer)),func.sum(cast(func.coalesce(vwdeselectpenalty.TotalDeselectDishCNT,0),sqlalchemy.Integer)),func.sum(cast(func.coalesce(vwdeselectpenalty.VendorPenaltyAMT,0),sqlalchemy.Integer)))
        etl = pd.read_sql_query("select format(sum(cast(e.[ETL_Payable] AS INT)),'C') AS etl_payable from ETL_credits e join msa m on e.FiberProvider=m.Vendor where m.msa_id = '{}'".format(request.form['fname']),con=conn)
        service_credits = pd.read_sql_query("select format(sum(cast(e.[Service Credits ($)] AS INT)),'C') AS service_credits from service_credits e join msa m on e.FiberProvider=m.vendor where m.msa_id = '{}'".format(request.form['fname']),con=conn)
        contract_value = pd.read_sql_query("select format(sum(cast(n.[monthly_MRC] as int)+cast(n.[monthly_NRC] AS int)),'c') as contract_value from [sourcing_financial_analysis] n join msa m on n.fiberprovider = m.vendor where m.msa_id = '{}'".format(request.form['fname']),con=conn)
        archive_section = pd.read_sql_query("select o.MSO_ID,o.Vendor,o.[MSO Effective Date],o.[Term (months)] AS Term,o.[Sites Added] from mso o join msa a on o.vendor=a.vendor where mso_id not in (select o.mso_id as sites from mso o join [New Vendor Sites] n on o.mso_id=n.[TT Contract Name] group by o.[mso_id] having count(n.[site id]) > 0 ) and a.msa_id = '{}' order by o.mso_id".format(request.form['fname']),con=conn)
        contract_info = MSA.query.filter(MSA.Contract_Name.in_(request.form['fname'].split()))
        MSO_info = MSO.query.filter(MSO.MSA_ID.in_(request.form['fname'].split()),MSO.ID.not_in(archive_section.MSO_ID.values.tolist())).order_by(MSO.ID)
        MSO_columns = list(MSO.__table__.columns)
        MSO_columns = MSO_columns[:-5]
        MPA_info = MPA.query.filter(MPA.MSA_ID.in_(request.form["fname"].split())).order_by(MPA.MPA_ID)
        MPA_columns = list(MPA.__table__.columns)
        MPA_columns = MPA_columns[:-7]
        return render_template("individual_MSAs.html",contract_info=contract_info,MSO_info=MSO_info,MSO_columns=MSO_columns,MPA_info=MPA_info,MPA_columns=MPA_columns,current_sites = current_sites_in_contract,original_sites = originally_awarded_sites,etl=etl,service_credits=service_credits,contract_value=contract_value,tables=[archive_section.to_html(classes='data')],titles=archive_section.columns.values,deselect_info=deselect_info)#,vendor_deselected_sites=vendor_deselected_sites,dish_deselected_sites=dish_deselected_sites)



@app.route('/MPA',methods=["GET","POST"])
def MPAs():
    if request.method == "GET":
        contracts = MPA.query.with_entities(MPA.MPA_ID).order_by(MPA.MPA_ID).all()
        return render_template('all_MPAs.html',contracts = contracts)
    if request.method=="POST":
        contract_info = MPA.query.filter(MPA.MPA_ID.in_(request.form['fname'].split()))
        sites  =contract_tool_v2.query.filter(contract_tool_v2.Contract_Name.in_(request.form["fname"].split())).order_by(contract_tool_v2.SiteID)
        return render_template('individual_MPAs.html',site_info = sites,contract_info = contract_info,columns = list(contract_tool_v2.__table__.columns))


@app.route('/contract_validation',methods=["GET","POST"])
def Contract_Validation():
    vendor_names = vendors.query.filter(
        vendors.vendor_type.in_(["Fiber Provider", "BOTH - Fiber & Data Center Vendor"]))
    if request.method == "GET":
        return render_template('contract_validation_upload.html',vendors=vendor_names)
    if request.method == 'POST':
        try:
            df = pd.read_excel(request.files.get('input_file'))
        except ValueError:
            df = pd.read_csv(request.files.get('input_file'))
        df.rename(columns={'site ID':'SiteID','siteID':'SiteID','Site ID':'SiteID','Z-Location':'SiteID','Z Location (Cell Site)':"Address",
                           'Service ID':'SiteID','Data Center':'DCAddress','Delivery Type':'LaunchCategory',
                           'A-Location':'DCAddress',"A Location (Data Center)":"DCAddress",'Z-Loc. LAT':'Lat','Z-Loc. LONG':'Long',
                           'Installation Delivery Intervals (days)':'DeliveryInterval',"Days from NTP":"Delivery Interval","DeliveryInterval":"Delivery Interval",'Initial Term':'Term',"Term (mo's)":"Term",
                           'Latitude (DEC)':'Lat','Longitude (DEC)':'Long','Data Center Address':'DCAddress','Notes':'Note'},inplace=True)
        df.MRC.replace({"\$": "",",":""},regex=True,inplace=True)
        df.NRC.replace({"\$": "",",":""}, regex=True, inplace=True)
        conn = create_engine("mssql+pyodbc:///?odbc_connect={}".format(connection_string), pool_pre_ping=True)
        df['RingID'] = df['SiteID'].str[:-1]
        df_copy = df.copy()
        if len(df.SiteID.tolist()) == 1:
            siteIDs = "('{}')".format(df.SiteID.tolist()[0])
            RingIDs = "('{}')".format(df.SiteID.str[:-1].tolist()[0])
        else:
            siteIDs = tuple(df.SiteID.tolist())
            RingIDs = tuple(df.SiteID.str[:-1].tolist())
        NXO_data = pd.read_sql_query("select siteid as SiteID,status as NXO_Status,lat as NXO_Lat,long as NXO_Long, DCAddress as NXO_DCAddress, LaunchCategory  as NXO_LaunchCategory,FiberProvider as NXO_FiberProvider, Address as NXO_Address, city as NXO_City, state as NXO_State, Zip as NXO_Zip from Source where siteid in {}".format(siteIDs),con=conn)
        NXO_data['NXO_Address'] = NXO_data["NXO_Address"].astype(str) + " " + NXO_data["NXO_City"].astype(str) + " " + NXO_data["NXO_State"].astype(str) + " " + NXO_data["NXO_Zip"].astype(str)
        NXO_data.drop(columns = ["NXO_City","NXO_State","NXO_Zip"],inplace=True)
        RingID_data = pd.read_sql_query("select siteid as SiteID_2,RingID, Status as Ring_Status, FiberProvider as Ring_FiberProvider from source where RingID in {} and status != 'Primary'  and FiberProvider is not null".format(RingIDs),con=conn)
        data_comparison = pd.merge(NXO_data,df,on='SiteID')
        data_comparison['Comparisons'] = ''
        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_Lat != data_comparison.Lat),'The Lat is wrong. ','')
        except:
            print("Can't evaluate the Latitudes.")
        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_Long != data_comparison.Long),
                                                      'The Long is wrong. ' + data_comparison['Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the Longitudes.")

        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_DCAddress != data_comparison.DCAddress),
                                                      'The DCAddress is wrong. ' + data_comparison['Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the DCAddress.")
        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_FiberProvider == data_comparison.NXO_FiberProvider),
                                                      'The site is awarded. ' + data_comparison['Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the Award status.")
        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_Status != 'Primary'),
                                                      'The Status is not Primary. ' + data_comparison['Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the site status.")
        try:
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_Address != data_comparison.Address),
                                                      'The site address is wrong. ' + data_comparison[
                                                          'Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the Address.")
        try:
            data_comparison.LaunchCategory = data_comparison.LaunchCategory.str.replace("P\&C","Performance/Capacity")
            data_comparison['Comparisons'] = np.where((data_comparison.NXO_LaunchCategory != data_comparison.LaunchCategory),
                                                      'The site LaunchCategory is wrong. ' + data_comparison[
                                                          'Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate LaunchCategory.")
        try:
            data_comparison['Comparisons'] = np.where(data_comparison.duplicated(subset=['SiteID']),'This site is a duplicate. ' + data_comparison[
                                                          'Comparisons'].astype(str),
                                                      '' + data_comparison['Comparisons'].astype(str))
        except:
            print("Can't evaluate the duplicates")


        data_comparison.Comparisons.replace('',np.nan,inplace=True)
        data_comparison.dropna(subset=['Comparisons'],inplace=True)
        df.drop(columns='RingID',inplace=True)

        vendor_name = request.form['VendorName']
        contract_type = request.form['contract_type']
        amd_contract_name = request.form['amd_contract_name']
        mpa_number = request.form['mpa_number']
        contract_name = naming_convention(vendor_name,contract_type,amd_contract_name,mpa_number)
        if data_comparison.empty:
            document = docx.Document(r'A:\My Drive\test\DISH - MSO - (DISTRIBUTION) - 12-08-22.docx')
            dfi.export(df,r'A:\My Drive\test\df.png')
            document.add_picture(r'A:\My Drive\test\df.png')
            document.save(secure_filename(r'\MSO_Template.docx'))
            contract_cost = df['MRC'].multiply(df['Term'],axis='index')+df['NRC'].astype(float).sum()
            df['Vendor'] = vendor_name
            df['Action'] = request.form['action']
            df['ContractName']  = contract_name
            df['IP_Address'] = request.remote_addr
            df.to_sql("contract_validation_test",if_exists='append',con=conn)
            return render_template('contract_validation_upload_success.html',vendors=vendor_names,contract_value=contract_cost,contract_name=contract_name)
        else:
            return render_template('contract_validation_rejection.html',
                                   tables=[data_comparison.to_html(classes='data')],
                                   titles=data_comparison.columns.values,vendors=vendor_names)


def as_dict(self):
    return {c.name: getattr(self, c.name) for c in self.__table__.columns}

@app.route('/test')
def rest_test():
    contract_info = MSA.query.filter(MSA.Contract_Name.in_(['VZN_MSA_001']))
    return  contract_info.__dict__
